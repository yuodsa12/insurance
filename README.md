선택 과제
-

결제요청을 받아 카드사와 통신하는 인터페이스를
제공하는 결제시스템(서버사전과제1_결제시스템)

개발환경
-

#### 환경 및 실행
spring boot 2.4.1
<br>
java 11
<br>
jpa & h2

* h2 DB 접속의 경우 path를 변경하지않았습니다.
(/h2-console로 접속가능)

* InsuranceApplication.main Run!!

#### 테이블 설계
 - 취소 테이블 : create table pay_cancel (id varchar(255) not null, payment_id varchar(255), result varchar(500), primary key (id)) engine=InnoDB
 - 결제 테이블 : create table pay_payment (id varchar(255) not null, result varchar(500), version integer, primary key (id)) engine=InnoDB
 - 결제 테이블 id <->취소 테이블 payment_id column  ( one to many )

* 테이블 설계 이유 : 카드번호, 결제금액 등 여러 테이블에 있을 시 관리 point 가 늘어날 수 있음(단점으로 entity 속성이 늘어나면 parsing이 힘들수있음... 히스토리용으로 string을 저장하고, 다른 테이블에 column마다 저장.. 하는 방식이 더 좋을 것 같음)

문제 해결 전략
-

1. 데이터 파싱
- annotation(https://gitlab.com/yuodsa12/insurance/-/blob/master/src/main/java/com/kakao/insurance/annotation/Parsing.java) 과 reflection(https://gitlab.com/yuodsa12/insurance/-/blob/master/src/main/java/com/kakao/insurance/util/Formatting.java)을 이용하여 dto 내에서 알아보기 쉽게 바로 정의하게끔 개발 

2. 트랜잭션
 - JPA에서 제공하는 낙관적 락 사용
 - 참고 : https://sup2is.github.io/2020/10/22/jpa-optimistic-lock-and-pessimistic-lock.html
 
3. validation
 - @Valid & ( @NotNull, @Min ) 등을 사용
 - @Valid을 사용하지않는 곳은 직접 validation

4. 에러처리
 - CustomType(https://gitlab.com/yuodsa12/insurance/-/blob/master/src/main/java/com/kakao/insurance/exception/ExceptionType.java)을 정의하여, API 응답에 대한 구체적 에러 정의 가능

5. 부분취소
초기 개발방향을 부분취소가 먼저 가능하게끔 로직개발
https://gitlab.com/yuodsa12/insurance/-/blob/master/src/test/java/com/kakao/insurance/PaymentServiceScenarioTest.java

API Interface
-

#### 결제 API
 - URL : /payment
 - method : POST
 - body :

 ```
[example]
 {
    "cardNumber" : 1234567890123456,
    "validDate" : 1125,
    "cvc" : 777,
    "payAmount" : 20000,
    "installments" : 0
}
 ```
  - result : 
```
{
    "id": "20210103022544VEFzNZ",
    "result": "_446PAYMENT___20210103022544VEFzNZ1234567890123456____001125777_____200000000001818____________________fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________",
    "cancelResults": null,
    "version": 0
}
```

#### 조회 API
 - URL : /find/{id}
 - method : GET
 - result : 
 
 ```
 
 {
    "type": "PAYMENT",
    "id": "20210102204746v9Ynzu",
    "cardNumber": 1234567890123456,
    "installments": 0,
    "validDate": 1125,
    "cvc": 777,
    "payAmount": 110000,
    "vat": 10000,
    "cancelId": null,
    "encryptedCardInforamtion": "fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=",
    "free": null,
    "cancelList": [
        {
            "type": "PAYMENT",
            "id": "20210102204812Fq4YV3",
            "cardNumber": 1234567890123456,
            "installments": 0,
            "validDate": 1125,
            "cvc": 777,
            "payAmount": 1000,
            "vat": 91,
            "cancelId": null,
            "encryptedCardInforamtion": null,
            "free": null,
            "cancelList": []
        }
    ]
}

```


#### 조회 API
 - URL : /cancel
 - method : POST
 - body : 
 ```
 {
    "id" : "2021010301505402Lxa7",
    "payAmount" : 10000
}
 ```
 - result :
 ```
 {
    "id": "20210103015111AyPUMJ",
    "paymentId": "2021010301505402Lxa7",
    "result": "CANCEL____20210103015111AyPUMJ1234567890123456____001125777_____1000000000008182021010301505402Lxa7fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"
}
 ``` 
