package com.kakao.insurance;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.entity.CancelResult;
import com.kakao.insurance.entity.PaymentResult;
import com.kakao.insurance.service.PaymentService;
import com.kakao.insurance.util.Formatting;
import com.kakao.insurance.util.PaymentUtil;

	
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PaymentControllerTest {
	@Autowired
	MockMvc mvc;
	  
	@Autowired 
	private WebApplicationContext ctx;
	
	@MockBean
	private PaymentService service;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private Payment payment;
	
	private Payment cancel;
	
	@BeforeEach
	public void init() {
		String id = PaymentUtil.getID();
		String cancelId = PaymentUtil.getID();
		
		payment = Payment.builder()
				.id(id)
				.cardNumber(1234567890123456L)
				.validDate(1125)
				.cvc(777)
				.installments(0)
				.payAmount(110000)
				.vat(1000)
				.build();
		
		cancel = Payment.builder()
				.id(id)
				.cancelId(cancelId)
				.payAmount(1000)
				.cardNumber(1234567890123456L)
				.validDate(1125)
				.cvc(777)
				.installments(0)
				.payAmount(110000)
				.vat(1000)
				.build();
		
		this.mvc = MockMvcBuilders
				.webAppContextSetup(ctx)
				.alwaysDo(print()) .build();

	}
	
	@DisplayName("[정상 호출]결제 요청")
	@Test
	public void success_pay_request() throws Exception {

		PaymentResult paymentResult = PaymentResult.builder()
				.id(this.payment.getId())
				.result(Formatting.formatting(payment))
				.build();
		
		given(service.createPaymentResult(any())).willReturn(paymentResult);
		
		mvc.perform(post("/payment")
				.content(objectMapper.writeValueAsString(this.payment))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(notNullValue())))
				.andExpect(jsonPath("$.result", is(notNullValue())))
				.andDo(print());
	}
	
	@DisplayName("[정상 호출]취소 요청")
	@Test
	public void success_cancel_request() throws Exception {
		CancelResult cancelResult = CancelResult.builder()
				.id(this.cancel.getId())
				.paymentId(this.cancel.getCancelId())
				.result(Formatting.formatting(cancel))
				.build();
		
		given(service.createCancelResult(any())).willReturn(cancelResult);
		
		mvc.perform(post("/cancel")
				.content(objectMapper.writeValueAsString(this.cancel))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.paymentId", is(notNullValue())))
				.andExpect(jsonPath("$.result", is(notNullValue())))
				.andDo(print());
	}
	
	@DisplayName("[정상 호출]조회 요청")
	@Test
	public void success_find_request() throws Exception {
		
		given(service.getPaymentBy(anyString())).willReturn(this.payment);
		
		mvc.perform(get("/find/" + payment.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(payment.getId())))
				.andDo(print());
	}
}
