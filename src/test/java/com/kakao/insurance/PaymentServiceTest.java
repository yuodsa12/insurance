package com.kakao.insurance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.entity.CancelResult;
import com.kakao.insurance.entity.PaymentResult;
import com.kakao.insurance.exception.CustomException;
import com.kakao.insurance.exception.ExceptionType;
import com.kakao.insurance.repository.CancelRepository;
import com.kakao.insurance.repository.PaymentRepository;
import com.kakao.insurance.service.PaymentService;

@SpringBootTest
public class PaymentServiceTest {
	@Autowired
	PaymentService service;
	
	@MockBean
	PaymentRepository paymentRepository;
	
	@MockBean
	CancelRepository cancelRepository;
	
	private Payment payment;
	private PaymentResult paymentResult;
	private String paymentId;
	private String paymentResultStr;
	
	private String cancelId;
	private CancelResult cancelResult;
	private String cancelResultStr;
	
	@BeforeEach
	void init() {
		/*
		 *  "cardNumber" : 1234567890123456,
		 *  "validDate" : 1125,
		 *  "cvc" : 777,
		 *  "payAmount" : 110000,
		 *  "installments" : 0,
		 *  "vat" : 1000
		 */
		paymentId = "20210102204746v9Ynzu";
		paymentResultStr = "_446PAYMENT___20210102204746v9Ynzu1234567890123456____001125777____1100000000001000____________________fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		paymentResult = PaymentResult.builder()
				.id(paymentId)
				.result(paymentResultStr)
				.build();
		
		payment = Payment.builder()
				.id(paymentId)
				.cardNumber(1234567890123456L)
				.validDate(1125)
				.cvc(777)
				.installments(0)
				.payAmount(110000)
				.vat(1000)
				.build();
		
		cancelId = "202101022048533YQl32";
		cancelResultStr = "_446PAYMENT___202101022048533YQl321234567890123456____001125777______1000000001000020210102204746v9Ynzufu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		cancelResult = CancelResult.builder()
				.id(cancelId)
				.paymentId(paymentId)
				.result(cancelResultStr)
				.build();
	}
	
	@DisplayName("결제건 만들기")
	@Test
	public void createPaymentResultTest() {
		given(paymentRepository.save(any())).willReturn(this.paymentResult);
		PaymentResult result = service.createPaymentResult(this.payment);
		assertEquals(result.getId(), this.paymentResult.getId());
		assertEquals(result.getResult(), this.paymentResult.getResult());
	}
	
	@DisplayName("결제건 가져오기")
	@Test
	public void getPaymentByIdTest() {
		given(paymentRepository.findById(anyString())).willReturn(Optional.of(this.paymentResult));
		
		Payment payment = service.getPaymentBy(this.paymentResult.getId());
		assertEquals(payment.getId(), paymentId);
		assertEquals(payment.getCardNumber(), 1234567890123456L);
		assertEquals(payment.getPayAmount(), 110000);
		assertEquals(payment.getCvc(), 777);
	}
	
	@DisplayName("[정상] 취소건 생성하기")
	@Test
	public void success_createCancelResultTest() throws Exception {
		Integer cancelAmount = 1000;
		Integer cancelVAT = 10;
		this.createCancelResult(cancelAmount, cancelVAT);
	}
	
	@DisplayName("[비정상, 취소 금액이 잔여결제액보다 많을 때] 취소건 생성하기")
	@Test
	public void fail_payment_createCancelResultTest() throws Exception {
		Integer cancelAmount = 1000000; // 잔여 결제액 : 110000
		Exception exception = assertThrows(CustomException.class, () -> this.createCancelResult(cancelAmount, null));
		
		String errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
	}
	
	@DisplayName("[비정상, 취소 VAT가 잔여 VAT보다 많을 때] 취소건 생성하기")
	@Test
	public void fail_vat_createCancelResultTest() throws Exception {
		Integer cancelAmount = 1000; // 잔여 결제액 : 110000
		Integer cancelVAT = 1000000; // 잔여 vat 1000
		Exception exception = assertThrows(CustomException.class, () -> this.createCancelResult(cancelAmount, cancelVAT));
		
		String errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
	}
	
	private void createCancelResult(Integer amount, Integer vat) throws Exception {
		given(paymentRepository.findById(anyString())).willReturn(Optional.of(this.paymentResult));
		given(cancelRepository.save(any())).willReturn(this.cancelResult);
		
		CancelResult result = service.createCancelResult(Payment.builder().id(this.paymentId).payAmount(amount).vat(vat).build());
		
		assertEquals(result.getId(), cancelResult.getId());
		assertEquals(result.getPaymentId(), cancelResult.getPaymentId());
		assertEquals(result.getResult(), cancelResult.getResult());
	}
}
