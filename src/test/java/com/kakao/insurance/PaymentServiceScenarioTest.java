package com.kakao.insurance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.entity.CancelResult;
import com.kakao.insurance.entity.PaymentResult;
import com.kakao.insurance.exception.CustomException;
import com.kakao.insurance.exception.ExceptionType;
import com.kakao.insurance.repository.CancelRepository;
import com.kakao.insurance.repository.PaymentRepository;
import com.kakao.insurance.service.PaymentService;
import com.kakao.insurance.util.Formatting;
import com.kakao.insurance.util.PaymentUtil;
import com.kakao.insurance.util.Type;

@SpringBootTest
public class PaymentServiceScenarioTest {
	@Autowired
	PaymentService service;
	
	@MockBean
	PaymentRepository paymentRepository;
	
	@MockBean
	CancelRepository cancelRepository;
	
	private PaymentResult paymentResult;
	private String paymentId;
	private String paymentResultStr;
	
	private Payment cancel;
	
	@BeforeEach
	void init() {
		/*
		 *  "cardNumber" : 1234567890123456,
		 *  "validDate" : 1125,
		 *  "cvc" : 777,
		 *  "payAmount" : 110000,
		 *  "installments" : 0,
		 *  "vat" : 1000
		 */
		paymentId = "20210102235402wIuFdD";
		paymentResult = PaymentResult.builder()
				.id(paymentId)
				.cancelResults(new ArrayList<CancelResult>())
				.build();
		
		cancel = Payment.builder()
				.id(paymentId)
				.build();
		
	}
	
	@DisplayName("[시나리오 1] 부분취소")
	@Test
	public void scenario1() throws Exception {
		// 결제 11000 , 1000
		paymentResultStr = "_446PAYMENT___20210102235402wIuFdD1234567890123456____001125777_____110000000001000____________________fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		this.paymentResult.setResult(paymentResultStr);
		given(paymentRepository.findById(anyString())).willReturn(Optional.of(this.paymentResult));
		
		// 부분 취소 1100, 100 (성공)
		String cancelIdAmount1100AndVAT100 = PaymentUtil.getID();
		this.cancel(cancelIdAmount1100AndVAT100, 1100, 100);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount1100AndVAT100, 1100, 100));
		
		// 부분 취소 3300, null (성공, vat 300원 자동 계산)
		String cancelIdAmount3300 = PaymentUtil.getID();
		this.cancel(cancelIdAmount3300, 3300, 300);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount3300, 3300, 300));
		
		// 부분 취소 7000, null (실패)
		String cancelIdAmount7000 = PaymentUtil.getID();
		Exception exception = assertThrows(CustomException.class, () -> this.cancel(cancelIdAmount7000, 7000, null));
		String errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
		
		// 부분 취소 6600, 700 (실패)
		String cancelIdAmount6600AndVAT700 = PaymentUtil.getID();
		exception = assertThrows(CustomException.class, () -> this.cancel(cancelIdAmount6600AndVAT700, 6600, 700));
		errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
		
		// 부분 취소 6600, 600 (성공)
		String cancelIdAmount6600AndVAT600 = PaymentUtil.getID();
		this.cancel(cancelIdAmount6600AndVAT600, 6600, 600);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount6600AndVAT600, 6600, 600));
		
		// 부분 취소 100, null (실패)
		String cancelIdAmount100 = PaymentUtil.getID();
		exception = assertThrows(CustomException.class, () -> this.cancel(cancelIdAmount100, 100, null));
		errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
	}
	
	@DisplayName("[시나리오 2] 부분취소")
	@Test
	public void scenario2() throws Exception {
		// 결제 20000 , 909
		paymentResultStr = "_446PAYMENT___20210102235402wIuFdD1234567890123456____001125777_____200000000000909____________________fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		this.paymentResult.setResult(paymentResultStr);
		given(paymentRepository.findById(anyString())).willReturn(Optional.of(this.paymentResult));
		
		// 부분 취소 10000, 0 (성공)
		String cancelIdAmount10000AndVAT0 = PaymentUtil.getID();
		this.cancel(cancelIdAmount10000AndVAT0, 10000, 0);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount10000AndVAT0, 10000, 0));
		
		// 부분 취소 10000, 0 (실패)
		String cancelIdAmount10000AndVAT0_2 = PaymentUtil.getID();
		Exception exception = assertThrows(CustomException.class, () -> this.cancel(cancelIdAmount10000AndVAT0_2, 10000, 0));
		String errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
		
		// 부분 취소 10000, 909 (성공)
		String cancelIdAmount10000 = PaymentUtil.getID();
		this.cancel(cancelIdAmount10000, 10000, 909);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount10000, 10000, 909));
	}
	
	@DisplayName("[시나리오 3] 부분취소")
	@Test
	public void scenario3() throws Exception {
		// 결제 20000 , null ( 자동계산 1818)
		paymentResultStr = "_446PAYMENT___20210102235402wIuFdD1234567890123456____001125777_____200000000001818____________________fu/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________/WM6jU1laqKl60PDqOetDtcSxMb6dvoK21YRaLgaY=_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		this.paymentResult.setResult(paymentResultStr);
		given(paymentRepository.findById(anyString())).willReturn(Optional.of(this.paymentResult));
		
		// 부분 취소 10000, 1000 (성공)
		String cancelIdAmount10000AndVAT0 = PaymentUtil.getID();
		this.cancel(cancelIdAmount10000AndVAT0, 10000, 1000);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount10000AndVAT0, 10000, 1000));
		
		// 부분 취소 10000, 909 (실패)
		String cancelIdAmount10000AndVAT0_2 = PaymentUtil.getID();
		Exception exception = assertThrows(CustomException.class, () -> this.cancel(cancelIdAmount10000AndVAT0_2, 10000, 909));
		String errorMessage = ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST.getErrorMessage();
		assertEquals(errorMessage, exception.getMessage());
		
		// 부분 취소 10000, null (성공)
		String cancelIdAmount10000 = PaymentUtil.getID();
		this.cancel(cancelIdAmount10000, 10000, null);
		this.paymentResult.getCancelResults().add(this.createCancelResult(cancelIdAmount10000, 10000, null));
	}
	
	private void cancel(String cancelId, Integer cancelAmount, Integer cancelVAT) throws Exception {
		this.cancel.setPayAmount(cancelAmount);
		this.cancel.setVat(cancelVAT);
		
		given(cancelRepository.save(any())).willReturn(CancelResult.builder()
				.id(cancelId)
				.paymentId(paymentId)
				.build());
		
		CancelResult result = service.createCancelResult(this.cancel);
		assertEquals(result.getId(), cancelId);
		assertEquals(result.getPaymentId(), paymentId);
	}
	
	private CancelResult createCancelResult(String cancelId, Integer amount, Integer vat) {
		Payment cancel =  Payment.builder()
				.type(Type.CANCEL.name())
				.id(cancelId)
				.cancelId(paymentId)
				.cardNumber(1234567890123456L)
				.validDate(1125)
				.cvc(777)
				.installments(0)
				.payAmount(amount)
				.vat(vat)
				.build();
		
		return CancelResult.builder()
				.id(cancelId)
				.paymentId(paymentId)
				.result(Formatting.formatting(cancel))
				.build();
	}
}
