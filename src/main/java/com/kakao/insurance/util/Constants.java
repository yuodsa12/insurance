package com.kakao.insurance.util;

public class Constants {
	public static class BLANK {
		public static final String UNDER_BAR = "_";
		public static final String ZERO = "0";
	}
	
	public static class ALIGN {
		public static final String LEFT = "left";
		public static final String RIGHT = "right";
	}
	
	public static class RATE {
		public static final Double VAT_RATE = 11.0;
	}
	
	public static class SEPERATOR {
		public static final Character CARD_INFO = '|';
		public static final Character ID = '*';
	}
}
