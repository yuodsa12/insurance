package com.kakao.insurance.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.util.Strings;

import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.util.Constants.RATE;
import com.kakao.insurance.util.Constants.SEPERATOR;

public class PaymentUtil {
	/**
	 * 현재시간과 알파벳&숫자 조합으로 20자리 ID 생성
	 * @return
	 */
	public static String getID() {
	    String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYYMMDDHHmmss"));
	    return now + RandomStringUtils.randomAlphanumeric(6);
	}
	
	/**
	 * 카드번호|유효기간|CVC 암호화
	 * @param payment
	 * @return
	 */
	public static String encryptCardInformation(Payment payment) {
		String formattedCardInformation = Strings.join(List.of(payment.getCardNumber(), payment.getValidDate(), payment.getCvc()), SEPERATOR.CARD_INFO);
		return CiperUtil.encrypt(formattedCardInformation);
	}
	
	/**
	 * VAT 계산
	 * @param amount
	 * @return
	 */
	public static Integer getVAT(Integer amount) {
		return (int) (Math.round((amount / RATE.VAT_RATE)));
	}
}
