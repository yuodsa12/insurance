package com.kakao.insurance.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;

import com.kakao.insurance.annotation.Parsing;
import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.util.Constants.ALIGN;
import com.kakao.insurance.util.Constants.BLANK;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Formatting {
	public static final String formatting(Payment payment) {
		Annotation annotation;
		String result = Strings.EMPTY;
		Field dataLength;
		Integer totalLength = 0;
		
		try {
			Field[] fields = payment.getClass().getDeclaredFields();
			if (ArrayUtils.isEmpty(fields)) {
				return result;
			}
			
			dataLength = fields[0];
			fields = Arrays.copyOfRange(fields, 1, fields.length);
			for(Field field : fields) {
				annotation = field.getAnnotation(Parsing.class);
				if (Objects.isNull(annotation)) continue;
				
				field.setAccessible(true);
				Object fieldValue = field.get(payment);
				String formatted = Formatting.formatting(fieldValue, annotation);
				result += formatted;
				totalLength += ((Parsing)annotation).size();
			}
			
			// data length
			payment.setDataLength(totalLength);
			annotation = dataLength.getAnnotation(Parsing.class);
			dataLength.setAccessible(true);
			Object fieldValue = dataLength.get(payment);
			String formattedDataLength = Formatting.formatting(fieldValue, annotation);
			result = formattedDataLength + result;
			
		} catch(Exception e) {
			log.error(e.toString());
		}
		
		return result;
	}
	
	/*
	 * 필드 하나씩 Parsing 정의에 따라 값 포맷팅 
	 */
	public static final String formatting(Object fieldValue, Annotation annotation) throws Exception {
		if (Objects.isNull(fieldValue)) {
			fieldValue = Strings.EMPTY;
		}
		
		int size = ((Parsing)annotation).size();
		String blank = ((Parsing)annotation).blank();
		String align = ((Parsing)annotation).align();
		size = Constants.ALIGN.RIGHT.equals(align) ? size : -size;
		
		return String.format("%" + size + "s", String.valueOf(fieldValue)).replace(" ", blank);
	}
	
	public static final Payment unformatting(String result) {
		Annotation annotation;
		Payment payment = Payment.builder().build();
		String parsingTarget = result;
		int pos = 0;
				
		try {
			Field[] fields = payment.getClass().getDeclaredFields();
			for(Field field : fields) {
				annotation = field.getAnnotation(Parsing.class);
				if (Objects.isNull(annotation)) continue;
				
				field.setAccessible(true);
				
				int size = ((Parsing)annotation).size();
				String fieldValue = parsingTarget.substring(pos, pos + size);
				String unformattingValue = Formatting.unformatting(fieldValue, annotation);
				if (Objects.isNull(unformattingValue)) continue;
				
				if (field.getType().equals(Long.TYPE)) {
					field.set(payment, Long.parseLong(unformattingValue));
				} else if (field.getType().equals(Integer.TYPE) || field.getType().equals(Integer.class)) {
					field.set(payment, Integer.parseInt(unformattingValue));
				} else {
					field.set(payment, unformattingValue);
				}
				
				pos += size;
			}
		} catch(Exception e) {
			log.error(e.getMessage());
		}
		
		return payment;
	}
	
	/*
	 * 필드 하나씩 Parsing 정의에 따라 값 추출 
	 */
	public static final String unformatting(String param, Annotation annotation) throws Exception {
		String blank = ((Parsing)annotation).blank();
		String align = ((Parsing)annotation).align();
		
		String result = param;
		if (ALIGN.RIGHT.equals(align)) {
			while(StringUtils.isNotBlank(result) &&
					StringUtils.equals(String.valueOf(result.charAt(0)), blank)) {
				result = result.substring(1, result.length());
			}
		} else {
			while(StringUtils.isNotBlank(result) &&
					StringUtils.equals(String.valueOf(result.charAt(result.length() - 1)), blank)) {
				result = result.substring(0, result.length() - 1);
			}
		}
		
		// 할부의 경우 0
		if (StringUtils.isBlank(result) && StringUtils.equals(BLANK.ZERO, blank)) {
			return BLANK.ZERO;
		}
			
		return StringUtils.isBlank(result) ? null : result;
	}
}