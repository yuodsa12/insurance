package com.kakao.insurance.exception;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum ExceptionType {
	INVALID_PARAMETER(400, "잘못된 파라미터가 있습니다."),
	NOT_FOUND_ENTITY(500, "결제 / 취소 이력을 찾을 수 없습니다."),
	NOT_ACCESS_PAYMENT(500, "현재 다른 결제건을 처리중입니다."),
	CAN_NOT_APPLY_CANCEL_REQUEST(500, "결제를 처리할 수 없습니다.");
	
	int errorCode;
    String errorMessage;

    ExceptionType(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public void AddSuffixMessage(String message) {
    	this.errorMessage = this.errorMessage + "[" + message + "]";
    }
}
