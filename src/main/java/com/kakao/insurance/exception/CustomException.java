package com.kakao.insurance.exception;

import lombok.Getter;

public class CustomException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	@Getter
    private ExceptionType type;

    public CustomException(ExceptionType type){
        super(type.getErrorMessage());
        this.type = type;
    }

}
