package com.kakao.insurance.exception;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(CustomException.class)
	public ExceptionType exception(CustomException e) {
		return e.getType();
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ExceptionType processValidationError(MethodArgumentNotValidException e) {
		String message = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
	    ExceptionType type = ExceptionType.INVALID_PARAMETER;
	    type.AddSuffixMessage(message);
	    return type;
	}
}
