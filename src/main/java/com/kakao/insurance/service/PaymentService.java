package com.kakao.insurance.service;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.stereotype.Service;

import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.entity.CancelResult;
import com.kakao.insurance.entity.PaymentResult;
import com.kakao.insurance.exception.CustomException;
import com.kakao.insurance.exception.ExceptionType;
import com.kakao.insurance.repository.CancelRepository;
import com.kakao.insurance.repository.PaymentRepository;
import com.kakao.insurance.util.Formatting;
import com.kakao.insurance.util.PaymentUtil;
import com.kakao.insurance.util.Type;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentService {
	private final PaymentRepository paymentRepository;
	private final CancelRepository cancelRepository;
	private final LockRegistry registry;
	private final RedisLockService redisLockService;
	
	/*
	 * 결제건 생성
	 */
	public PaymentResult createPaymentResult(Payment payment) {
		return paymentRepository.save(PaymentResult.builder()
				.id(payment.getId())
				.result(Formatting.formatting(payment))
				.build());
	}
	
	/*
	 * 관리ID를 기준으로 결제/취소건을 가져옴
	 */
	public Payment getPaymentBy(String id) {
		PaymentResult paymentResult = paymentRepository.findById(id)
				.orElseThrow(() -> new CustomException(ExceptionType.NOT_FOUND_ENTITY));
		
		Payment payment = Formatting.unformatting(paymentResult.getResult());
		payment.setEncryptedCardInforamtion(PaymentUtil.encryptCardInformation(payment));
		
		if (Objects.nonNull(paymentResult.getCancelResults())) {
			payment.setCancelList(
					paymentResult.getCancelResults().stream()
					.map(result -> Formatting.unformatting(result.getResult()))
					.collect(Collectors.toList()));			
		}
	
		return payment;
	}
	
	/*
	 * 취소건 생성
	 */
	@Transactional
	public CancelResult createCancelResult(Payment param) throws Exception {
		
		/*Lock lock = registry.obtain(param.getId());
		boolean acquired = lock.tryLock(1, TimeUnit.SECONDS);
		if (!acquired) {
			throw new CustomException(ExceptionType.NOT_ACCESS_PAYMENT);
		}*/
		
		/* redis lock test */
		boolean redisAcquired = redisLockService.tryLock(param.getId(), 1);
		if (!redisAcquired) {
			throw new CustomException(ExceptionType.NOT_ACCESS_PAYMENT);
		}
		
		try {
			Payment payment = this.getPaymentBy(param.getId());
			
			/* [Start]Amount Validation */
			int cancelAmountByRequest = param.getPayAmount();
			int totalCancelAmount = payment.getCancelList().stream().mapToInt(cancel -> cancel.getPayAmount()).sum();
			int restPayAmount = payment.getPayAmount() - totalCancelAmount;
			
			if (restPayAmount - cancelAmountByRequest < 0) {
				throw new CustomException(ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST);
			}
			/* [End]Amount Validation */
			
			/* [Start]VAT Validation */
			Integer totalCancelVat = payment.getCancelList().stream().mapToInt(cancel -> cancel.getVat()).sum();
			Integer restVat = payment.getVat() - totalCancelVat;
			Integer cancelVatByRequest = param.getVat();
			
			Boolean vatNotNullAndVatDiffMinus = Objects.nonNull(cancelVatByRequest) && (restVat - cancelVatByRequest < 0);
			Boolean vatNotNullAndAmountDiffZeroAndVatDiffPlus = 
					Objects.nonNull(cancelVatByRequest) 
					&& (restPayAmount - cancelAmountByRequest == 0) 
					&& (restVat - cancelVatByRequest > 0);
			
			if (vatNotNullAndVatDiffMinus || vatNotNullAndAmountDiffZeroAndVatDiffPlus) {
				throw new CustomException(ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST);
			} else if (Objects.isNull(cancelVatByRequest)) {
				cancelVatByRequest = PaymentUtil.getVAT(cancelAmountByRequest);
				cancelVatByRequest = restVat - cancelVatByRequest >= 0 ? cancelVatByRequest : restVat;
			}
			/* [End]VAT Validation */
			
			/* 취소건 생성 */
			String cancelId = PaymentUtil.getID();
			payment.setType(Type.CANCEL.name());
			payment.setPayAmount(cancelAmountByRequest);
			payment.setVat(cancelVatByRequest);
			payment.setCancelId(payment.getId());
			payment.setId(cancelId);
			
			return cancelRepository.save(CancelResult.builder()
					.id(cancelId)
					.paymentId(param.getId())
					.result(Formatting.formatting(payment))
					.build());
			
		} catch(Exception e) {
			log.error(e.getLocalizedMessage());
			throw new CustomException(ExceptionType.CAN_NOT_APPLY_CANCEL_REQUEST);
		} finally {
//			lock.unlock();
			
			/* redis lock test */
			redisLockService.unlock(param.getId());
		}
	}
}
