package com.kakao.insurance.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.kakao.insurance.annotation.Parsing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@Entity(name = "pay_payment")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaymentResult {
	@Id
	@Parsing(size = 20)
	String id;
	
	@Column(length = 500)
	String result;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "payment_id")
	List<CancelResult> cancelResults;
	
	@Version
	Integer version;
}
