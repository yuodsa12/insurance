package com.kakao.insurance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.kakao.insurance.annotation.Parsing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@Entity(name = "pay_cancel")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CancelResult {
	@Id
	@Parsing(size = 20)
	String id;
	
	@Parsing(size = 20)
	@Column(name = "payment_id")
	String paymentId;
	
	@Column(length = 500)
	String result;
}
