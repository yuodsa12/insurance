package com.kakao.insurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kakao.insurance.entity.CancelResult;

public interface CancelRepository extends JpaRepository<CancelResult, String>{

}
