package com.kakao.insurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kakao.insurance.entity.PaymentResult;

public interface PaymentRepository extends JpaRepository<PaymentResult, String> {
}
