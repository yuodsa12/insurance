package com.kakao.insurance.config;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.stereotype.Component;

@Component
public class SpringIntegertaionRedisLockConfig {
	@Bean(destroyMethod = "destroy")
	public RedisLockRegistry redisLockRegistry(RedisConnectionFactory redisConnectionFactory) {
	    return new RedisLockRegistry(redisConnectionFactory, "lock");
	}
}
