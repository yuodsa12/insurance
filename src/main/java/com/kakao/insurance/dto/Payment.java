package com.kakao.insurance.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.kakao.insurance.annotation.Parsing;
import com.kakao.insurance.util.Constants.ALIGN;
import com.kakao.insurance.util.Constants.BLANK;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Payment {
	
	@Parsing(size = 4, align = ALIGN.RIGHT)
	int DataLength;
	
	@Parsing(size = 10)
	String type;

	@Parsing(size = 20)
	String id;
	
	@Parsing(size = 20)
	@NotNull(message = "카드번호는 필수입니다.")
	@Min(value = 1000000000, message = "카드번호는 10자리 이상이여야합니다.")
	@Digits(integer = 16, fraction = 0, message = "카드번호는 최대 16자리입니다.")
	long cardNumber;
	
	@Parsing(size = 2, blank = BLANK.ZERO)
	int installments;
	
	@Parsing(size = 4)
	@NotNull(message = "카드 유효일자는 필수입니다.")
	int validDate;
	
	@Parsing(size = 3)
	@NotNull(message = "카드 CVC는 필수입니다.")
	int cvc;
	
	@Parsing(size = 10, align = ALIGN.RIGHT)
	@Min(value = 100, message = "요청 금액은 최소 100원 이상이여야합니다.")
	@NotNull(message = "요청 금액은 필수입니다.")
	int payAmount;
    
	@Parsing(size = 10, blank = BLANK.ZERO, align = ALIGN.RIGHT)
	Integer vat;
	
	@Parsing(size = 20)
	String cancelId;
	
	@Parsing(size = 300)
    String encryptedCardInforamtion;
	
	@Parsing(size = 47)
	String free;
	
	List<Payment> cancelList;
	
	public List<Payment> getCancelList() {
		if (Objects.isNull(this.cancelList)) {
			this.cancelList = new ArrayList<Payment>();
		}
		
		return this.cancelList;
	}
}
