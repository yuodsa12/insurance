package com.kakao.insurance.controller;

import java.util.Objects;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kakao.insurance.dto.Payment;
import com.kakao.insurance.entity.CancelResult;
import com.kakao.insurance.entity.PaymentResult;
import com.kakao.insurance.exception.CustomException;
import com.kakao.insurance.exception.ExceptionType;
import com.kakao.insurance.service.PaymentService;
import com.kakao.insurance.util.PaymentUtil;
import com.kakao.insurance.util.Type;

@RestController
public class PaymentController {
	
	@Autowired
	private PaymentService service;
	
	/**
	 * 결제 요청
	 * @param param
	 * @return
	 */
	@PostMapping("payment")
	public PaymentResult payment(@Valid @RequestBody Payment param) {
		param.setId(PaymentUtil.getID());
		param.setType(Type.PAYMENT.name());
		param.setEncryptedCardInforamtion(PaymentUtil.encryptCardInformation(param));
		if (Objects.isNull(param.getVat())) {
			param.setVat(PaymentUtil.getVAT(param.getPayAmount()));
		}
		
		return service.createPaymentResult(param);
	}
	
	/**
	 * 취소 요청
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@PostMapping("cancel")
	public CancelResult cancel(@RequestBody Payment param) throws Exception {
		if (StringUtils.isBlank(param.getId())) {
			ExceptionType type = ExceptionType.INVALID_PARAMETER;
			type.AddSuffixMessage("관리번호는 필수입니다.");
			throw new CustomException(type);
		}
	
		return service.createCancelResult(param);
	}
	
	/**
	 * 조회
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("find/{id}")
	public Payment find(@PathVariable(name = "id", required = true) String id) {
		return service.getPaymentBy(id);
	}
}
