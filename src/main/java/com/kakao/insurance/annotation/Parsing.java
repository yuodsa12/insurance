package com.kakao.insurance.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.kakao.insurance.util.Constants.ALIGN;
import com.kakao.insurance.util.Constants.BLANK;


/*
 * 참고 https://blog.gangnamunni.com/post/Annotation-Reflection-Entity-update/
 * 카드사와의 통신 규격 포맷팅
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Parsing {
	int size() default 0;
	String blank() default BLANK.UNDER_BAR;
	String align() default ALIGN.LEFT; 
}
